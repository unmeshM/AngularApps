(function(angular) {
    'use strict';

    angular.module('Router', [ 'ui.router', Router]);

    function Router(AppService) {
        return {
            init:init
        };

        function getRoutes() {
            var routes = AppService.getData('app.routes.json');
            if (routes == null
                || routes == []) {
                throw "routes not found";
            }
            return routes;
        }

        function init() {
            try {
                var routes = getRoutes();
                angular.forEach(routes, function(value, key) {
                    var state = {
                        "url": value.url,
                        "parent": value.parent,
                        "abstract": value.abstract,
                        "views": {}
                    };
                    angular.forEach(value.views, function(view) {
                        state.views[view.name] = {
                            templateUrl: view.templateUrl,
                            controller: view.controller
                        };
                    });

                    $stateProvider.state(value.name, state);
                });
                $state.go("product");
            }
            catch (error) {
                $log.error(error);
            }
        }
    }
})(angular);