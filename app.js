(function(angular) {

    'use strict';

    //define the app module
    var HWApp=angular.module('HWApp',['ui.router']);

    //HWApp.run(Router.init());

    var $stateProviderRef;
    var $urlRouterProviderRef;
    //app configuration
    HWApp.config(["$locationProvider", "$stateProvider", "$urlRouterProvider", 
    function($locationProvider,$stateProvider,$urlRouterProvider) {
        $urlRouterProviderRef = $urlRouterProvider;

        $locationProvider.html5Mode(false);
        $stateProviderRef = $stateProvider;

    }]);

    HWApp.run(['$q', '$rootScope', '$state', '$http',
        function($q, $rootScope, $state, $http) {
            $http.get("app.routes.json")
                .success(function(data) {
                    angular.forEach(data, function(value, key) {
                        var state = {
                            "url": value.url,
                            "parent": value.parent,
                            "abstract": value.abstract,
                            "views": {}
                        };                     
                            angular.forEach(value.views, function(view) {
                                state.views[view.name] = {
                                    templateUrl: view.templateUrl,
                                    controller:view.controller
                                };
                            });

                            $stateProviderRef.state(value.name, state);
                        });
                    $state.go("product");
                });
        }
    ]); 
})(angular);