(function(angular) {
    'use strict';

    angular.module('HWApp')
    .service('AppService', ['$http',AppService]);

    function AppService($http) {
        return{
            getData:getData
        };
        
        function getData(url) {
            $http.get(url)
                .success(function(data) {
                    return data;
                });
        }
    }
})(angular);